import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);
	
		int turn = 0;
		
		while (turn < 9) {
			
			if (turn % 2 == 0) {
				
				System.out.print("Player 1 enter row number:");
				int row = reader.nextInt();

				System.out.print("Player 1 enter column number:");
				int col = reader.nextInt();
				if (row < 1 || row > 3 || col < 1 || col > 3 || board[row - 1][col - 1] != ' ') {
					
					System.out.println("invalid coordinates, try again");
					continue;
					
				}
				board[row - 1][col - 1] = 'X';
				if (isOver(board)) {
					System.out.println("Player 1 won!!");
					break;
				}
				printBoard(board);
				if (isOver(board)) {	
					
				}
				turn++;
				
			} else {
				
				System.out.print("Player 2 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				int col = reader.nextInt();
				if (row < 1 || row > 3 || col < 1 || col > 3 || board[row - 1][col - 1] != ' ') {			
					System.out.println("invalid coordinates, try again");
					continue;			
				}
				board[row - 1][col - 1] = 'O';
				printBoard(board);
				if (isOver(board)) {
					System.out.println("Player 2 won!!");
					break;
				}	
				turn++;
			}
			if (turn == 9) {
				System.out.println("draw");
			}
			
		}
		reader.close();
	}
	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}
	
	static boolean isOver(char[][] arr) {
		
		if (arr[0][1] == arr[0][2] && arr[0][0] == arr[0][1] && (arr[0][1] == 'X' || arr[0][1] == 'O')) {
			return true;
		}
		if (arr[1][1] == arr[1][2] && arr[1][0] == arr[1][1] && (arr[1][1] == 'X' || arr[1][1] == 'O')) {
			return true;
		}
		if (arr[2][1] == arr[2][2] && arr[0][0] == arr[2][1] && (arr[2][1] == 'X' || arr[2][1] == 'O')) {
			return true;
		}
		if (arr[0][0] == arr[1][0] && arr[2][0] == arr[1][0] && (arr[0][0] == 'X' || arr[0][0] == 'O')) {
			return true;
		}
		if (arr[0][1] == arr[1][1] && arr[2][1] == arr[1][1] && (arr[0][1] == 'X' || arr[0][1] == 'O')) {
			return true;
		}
		if (arr[0][2] == arr[1][2] && arr[2][2] == arr[1][2] && (arr[0][2] == 'X' || arr[0][2] == 'O')) {
			return true;
		}
		if (arr[0][0] == arr[1][1] && arr[0][0] == arr[2][2] && (arr[0][0] == 'X' || arr[0][0] == 'O')) {
			return true;
		}
		if (arr[2][0] == arr[1][1] && arr[2][0] == arr[0][2] && (arr[1][1] == 'X' || arr[1][1] == 'O')) {
			return true;
		}
		return false;
	}

}